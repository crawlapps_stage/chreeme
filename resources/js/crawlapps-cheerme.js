const host = process.env.MIX_APP_URL;
const apiEndPoint = host + '/api';

var shopifyDomain = Shopify.shop;
var jqueryLoaded = 0;

if(!window.jQuery)
{
    var script = document.createElement('script');
    script.type = "text/javascript";
    script.src = "https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js";
    document.getElementsByTagName('head')[0].appendChild(script);
    jqueryLoaded = 1;
}

Window.crawlapps_chreeme = {
    shop_data: JSON.parse(document.getElementById('crawlapps_cheerme_shop_data').innerHTML),
    init: function () {
        this.initScript();
    },
    initScript() {

        Window.crawlapps_chreeme.getPublicKey();
    },
    getPublicKey() {
        var self = Window.crawlapps_chreeme;
        let aPIEndPoint = `${apiEndPoint}/${shopifyDomain}/get-publickey`;
        $.ajax({
            method: "GET",
            url: aPIEndPoint,
            contentType: 'application/json;',
            success:function (response,success,header) {
                $('body').append("<div id='cheerme-launcher'></div>");
                window.cheerMeConfig = {
                    publicKey: response.data.public_key,
                    customerId: self.shop_data.customer.id?self.shop_data.customer.id.toString():"",
                    customerToken: 'I am customer',
                };
                var script = document.createElement('script');
                script.async = true;
                script.charset = "utf-8";
                script.src = "https://cdnfrontend.s3.ap-south-1.amazonaws.com/CDN/cheerme-prod/cheerme.min.js";
                document.getElementsByTagName('head')[0].appendChild(script);
            },
        });
    },
};

$(document).ready(function () {
    Window.crawlapps_chreeme.init();
});
