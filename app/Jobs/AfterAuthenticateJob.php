<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AfterAuthenticateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $shop;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($shop = null)
    {
        $this->shop = $shop;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::Info("=============== AfterAuthenticateJob ==================");
        if(is_null($this->shop))
            $shop = \ShopifyApp::shop();
        else
            $shop = $this->shop;

        $main_theme = $shop->api()->rest('GET', 'admin/themes.json',['role' => 'main']);
        $main_theme = $main_theme->body->themes[0]->id;
        $this->snippet($shop, $main_theme);
        $this->updateThemeLiquid($shop, $main_theme);
    }

    public function snippet($shop, $theme_id){

        $value = <<<EOF
        <script id="crawlapps_cheerme_shop_data" type="application/json">
            {
                "shop": {
                    "domain": "{{ shop.domain }}",
                    "permanent_domain": "{{ shop.permanent_domain }}",
                    "url": "{{ shop.url }}",
                    "secure_url": "{{ shop.secure_url }}",
                    "money_format": {{ shop.money_format | json }},
                    "currency": {{ shop.currency | json }}
                },
                "customer": {
                    "id": {{ customer.id | json }},
                    "tags": {{ customer.tags | json }}
                },
                "template": "{{ template | split: "." | first }}"
            }
        </script>
EOF;

        $parameter['asset']['key'] = 'snippets/crawlapps-cheerme.liquid';
        $parameter['asset']['value'] = $value;
        $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json',$parameter);
    }

    public function updateThemeLiquid($shop,$theme_id){
        $asset = $shop->api()->rest('GET', 'admin/themes/'.$theme_id.'/assets.json',["asset[key]" => 'layout/theme.liquid']);
        if(@$asset->body->asset) {
            $asset = $asset->body->asset->value;
            if(!strpos($asset ,"{% include 'crawlapps-cheerme' %}</head>")) {
                $asset = str_replace('</head>',"{% include 'crawlapps-cheerme' %}</head> ",$asset);
            }

            $parameter['asset']['key'] = 'layout/theme.liquid';
            $parameter['asset']['value'] = $asset;
             $shop->api()->rest('PUT', 'admin/themes/'.$theme_id.'/assets.json',$parameter);
        }

    }
}
