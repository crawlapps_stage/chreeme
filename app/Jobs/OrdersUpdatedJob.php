<?php namespace App\Jobs;

use App\Models\Shop;
use App\Models\ShopifyWebhook;
use App\Traits\HelperTrait;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class OrdersUpdatedJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, HelperTrait;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;
    public $tries = 3;
    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::Info("=============== Orders Updated Webhook ==================");
        $shop = Shop::where('shopify_domain',$this->shopDomain)->first();
        $data = json_encode($this->data);
        $order = json_decode($data);
        $shopify_id = $order->id;
        $financial_status = $order->financial_status;
        $cancelled_at = $order->cancelled_at;

        $entity = ShopifyWebhook::updateOrCreate(
            ['shopify_id' => $shopify_id, 'topic' => 'orders/updated', 'shop_id' => $shop->id],
            ['shopify_id' => $shopify_id, 'topic' => 'orders/updated', 'shop_id' => $shop->id, 'data' => $data, 'is_executed' => 0]
        );
        $status = ["refunded", "voided"];
        if(in_array($financial_status, $status) || !is_null($cancelled_at))
            $this->deleteOrder($entity->id, $shop);
        else
            $this->createUpdateOrder($entity->id, $shop);
            
            return true;
    }
}
