<?php

namespace App\Jobs;

class AppUninstalledJob extends \OhMyBrew\ShopifyApp\Jobs\AppUninstalledJob
{

    public function handle()
    {
        \Log::info('--------------- app uninstall ---------------------');

        if (!$this->shop) {
            return false;
        }
        $this->deleteRecords();
        $this->cancelCharge();
        $this->cleanShop();
        $this->softDeleteShop();
    }
}
