<?php namespace App\Jobs;

use App\Models\Shop;
use App\Models\ShopifyWebhook;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Traits\HelperTrait;

class OrdersDeleteJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels,HelperTrait;

    /**
     * Shop's myshopify domain
     *
     * @var string
     */
    public $shopDomain;

    /**
     * The webhook data
     *
     * @var object
     */
    public $data;
    public $tries = 3;
    /**
     * Create a new job instance.
     *
     * @param string $shopDomain The shop's myshopify domain
     * @param object $data    The webhook data (JSON decoded)
     *
     * @return void
     */
    public function __construct($shopDomain, $data)
    {
        $this->shopDomain = $shopDomain;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::Info("=============== Orders Delete Webhook ==================");
        $shop = Shop::where('shopify_domain',$this->shopDomain)->first();
        $data = json_encode($this->data);
        $shopify_id = json_decode($data)->id;

        $entity = ShopifyWebhook::updateOrCreate(
            ['shopify_id' => $shopify_id, 'topic' => 'orders/delete', 'shop_id' => $shop->id],
            ['shopify_id' => $shopify_id, 'topic' => 'orders/delete', 'shop_id' => $shop->id, 'is_executed' => 0]
        );
        $this->deleteOrder($entity->id, $shop);
        return true;
    }
}
