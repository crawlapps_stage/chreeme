<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingRequest;
use App\Models\Setting;
use Illuminate\Http\Request;
use OhMyBrew\ShopifyApp\ShopifyApp;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shop = \ShopifyApp::shop();
        $settingModel = Setting::where('shop_id',$shop->id)->first();
        return response($settingModel,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SettingRequest $request)
    {
        try{
            \DB::beginTransaction();
            $shop = \ShopifyApp::shop();
            $settingModel = Setting::updateOrCreate(
                ['shop_id' => $shop->id],
                ['public_key' => $request->public_key, 'private_key' => $request->private_key, 'shop_id' => $shop->id]
            );
            \DB::commit();
            $code = 200;
            $message = 'Successfully Saved.';
        }catch (\Exception $e){
            \DB::rollback();
            $code = $e->getCode();
            $message = $e->getMessage();
        }


        return response([
            'message' => $message
        ],$code);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
