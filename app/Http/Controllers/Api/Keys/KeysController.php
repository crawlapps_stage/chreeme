<?php

namespace App\Http\Controllers\Api\Keys;

use App\Http\Controllers\Controller;
use App\Models\Shop;
use Illuminate\Http\Request;


class KeysController extends Controller
{
    /**
     * @param  Request  $request
     * @param $shopifyShop
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPublicKey(Request $request, $shopifyShop){
        $shop = Shop::where('shopify_domain', $shopifyShop)->first();
        $response['public_key'] = $shop->getSettings->public_key;
        return response()->json(['data' => $response]);
    }
}
