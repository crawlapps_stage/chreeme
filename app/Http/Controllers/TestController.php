<?php

namespace App\Http\Controllers;

use App\Models\Shop;
use App\Traits\HelperTrait;
use Illuminate\Http\Request;


class TestController extends Controller
{
    use HelperTrait;

    public function test()
    {
        $this->deleteOrder(26, Shop::find(1));
    }
}
