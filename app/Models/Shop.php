<?php

namespace App\Models;

use OhMyBrew\ShopifyApp\Models\Shop as Base;
class Shop extends Base
{
    public function getSettings(){
        return $this->hasOne(Setting::class,'shop_id','id');
    }
}
