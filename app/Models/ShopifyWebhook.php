<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopifyWebhook extends Model
{
    protected $fillable = ["shopify_id", "topic", "shop_id", "data", "created_at", "updated_at","id", "is_executed"];

    public function shop(){
        return $this->belongsTo(Shop::class);
    }
}
