<?php namespace App\Traits;

use App\Models\ShopifyWebhook;
use App\Models\Setting;
trait HelperTrait{

    public function createUpdateCustomer($entity_id, $shop){

        \Log::info("---------------- 1");
        \Log::info($shop->shopify_domain);
        $settingModel = Setting::where('shop_id',$shop->id)->first();
        if(!is_null($settingModel)) {
            \Log::info("---------------- 2");
            $entity = ShopifyWebhook::find($entity_id);
            $data = json_decode($entity->data, 1);

            $baseURL = config('app.cheerme_api_url');
            $endpoint = $baseURL."/api/Public/Customer/CreateOrUpdate";
            $params = [];
            $params['ExternalId'] = (string) @$data['id'];
            $params['FirstName'] = @$data['first_name'];
            $params['LastName'] = @$data['last_name'];
            $params['Email'] = @$data['email'];
            $params['ExtenalCreatedAt'] = date("Y-m-d H:i:s", strtotime(@$data['created_at']));
            $params['ExternalUpdatedAt'] = date("Y-m-d H:i:s", strtotime(@$data['updated_at']));

            $params = http_build_query($params);
            $make_call = $this->callAPI('POST', $endpoint, $params, $shop, $settingModel);
            $response = json_decode($make_call, true);
            \Log::info("---------------- 3");
            \Log::info(json_encode($response));

            $entity->is_executed = 1;
            $entity->save();
        }
        return true;
    }
    public function callAPI($method, $url, $data, $shop, $settingModel = null){

        if($method == "DELETE"){
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_HTTPHEADER => array(
                    "PrivateKey: ".@$settingModel->private_key,
                    "Content-Type: application/x-www-form-urlencoded",
                    "Content-Length: 0"
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            return $response;
        }


        $curl = curl_init();
        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'PrivateKey: '.@$settingModel->private_key
        ));

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }
    public function createUpdateOrder($entity_id, $shop){
    \Log::info("createUpdateOrder Called ");
    \Log::info($shop->shopify_domain);
    $settingModel = Setting::where('shop_id',$shop->id)->first();
        if(!is_null($settingModel)) {

            $entity = ShopifyWebhook::find($entity_id);

            \Log::info($entity->topic);
            \Log::info($entity->created_at ."==". $entity->updated_at);
            if($entity->topic == "orders/updated" && $entity->created_at == $entity->updated_at) {
                \Log::info("In return condition: ");
                $entity->is_executed = 1;
                $entity->save();
                return true;
            }
                $data = json_decode($entity->data,1);
            $CouponCodes = "";
            if(count($data['discount_codes']) > 0){
                $CouponCodes = collect($data['discount_codes'])->pluck('code')->toArray();
            }

                $baseURL = config('app.cheerme_api_url');
                $endpoint = $baseURL."/api/Public/Order/CreateOrUpdate";
                $params = [];
                $params['ExternalId'] = (string) @$data['id'];
                $params['GrandTotal'] = @$data['total_price'];
                $params['SubTotal'] = @$data['subtotal_price'];
                $params['RewardableTotal'] = @$data['total_price'];
                $params['CustomerExternalId'] = (string) @$data['customer']['id'];
                $params['PaymentStatus'] = @$data['financial_status'];
                $params['CouponCodes'] = @$CouponCodes;
                $params['ExternalCreatedAt'] = date("Y-m-d H:i:s", strtotime(@$data['created_at']));
                $params['ExternalUpdatedAt'] = date("Y-m-d H:i:s", strtotime(@$data['updated_at']));

                \Log::info("Request JSON: ". json_encode($params));

                $params = http_build_query($params);
                $make_call = $this->callAPI('POST', $endpoint, $params, $shop,$settingModel);
                $response = json_decode($make_call, true);


                \Log::info("API Response: ". json_encode($response));

                $entity->is_executed = 1;
                $entity->save();

        }
        return true;
    }

    public function deleteOrder($entity_id, $shop){
        \Log::info("deleteOrder Called ");
        \Log::info($shop->shopify_domain);
        $settingModel = Setting::where('shop_id',$shop->id)->first();
        if(!is_null($settingModel)) {
            $entity = ShopifyWebhook::find($entity_id);
            $id = (string) $entity->shopify_id;
            $baseURL = config('app.cheerme_api_url');
            $endpoint = $baseURL."/api/public/Order/Delete?externalid=".$id;
            \Log::info("Url : ". json_encode($endpoint));
            $make_call = $this->callAPI('DELETE', $endpoint, null , $shop,$settingModel);
            \Log::info("API Response: ". $make_call);
            $entity->is_executed = 1;
            $entity->save();
        }
        return true;
    }
}



