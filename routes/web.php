<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('layouts.app');
})->middleware(['auth.shop'])->name('home');

Route::group(['middleware' => 'auth.shop'], function () {
    Route::resource('setting', 'Setting\SettingController');
});

Route::get('flush', function(){
    request()->session()->flush();
});


Route::get('test', 'TestController@test');
