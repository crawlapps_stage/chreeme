<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function () {

    Route::get('version', function(Request $request) {

        $jsFilePath = public_path() . '/js/crawlapps-cheerme.js';
        $jsVersion  = filemtime($jsFilePath);

        return response()->json(['data' => ['js' => $jsVersion]]);
    });

    Route::group(['prefix' => '{shopifyShop}'], function () {
        Route::get('/get-publickey', 'Keys\KeysController@getPublicKey');

    });
});
